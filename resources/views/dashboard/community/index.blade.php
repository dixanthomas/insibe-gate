@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $communities->count() }} Communitys.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/community/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Community</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Community ID</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text"> Community Name</span></div>
                {{-- <div class="nk-tb-col"><span class="sub-text">community Flat Number</span></div>
                <div class="nk-tb-col"><span class="sub-text">community Mobile</span></div>
                <div class="nk-tb-col"><span class="sub-text">community Email Id</span></div> --}}
            </div><!-- .nk-tb-item -->
            @if(count($communities ) > 0)
                @foreach($communities as $community)
                    <div class="nk-tb-item">
                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/community/'.$community->id.'/edit') }}">
                                <span class="tb-lead">   {{ $community->id }} </span>
                            </a>
<!-- {{--                            @foreach($tender->categories as $category )--}}
{{--                                <a href="{{ url('dashboard/categories/'.$category->id.'/edit') }}">--}}
{{--                                    <span class="badge badge-secondary">{{ $category->title }}</span>--}}

{{--                                </a>--}}

{{--                            @endforeach--}} -->
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $community->name }}</span>
                        </div>
                        {{-- <div class="nk-tb-col tb-col-lg">
                            <span> {{ $community->flat }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $community->mobile }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $community->email }}</span>
                        </div> --}}
                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    <!-- {{ $communities->links() }} -->

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection
