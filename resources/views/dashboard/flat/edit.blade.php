{{-- @extends('layouts.guest-dashboard') --}}
@extends('layouts.guest-dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">
                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                {{-- <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Go Back</span></a> --}}
                                <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><span>Go Back</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">  
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($flat, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}

                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >City <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('city',[''=>'',$city] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select City','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Society <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('society',[''=>'',$society] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Society','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Tower<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('tower',[''=>'',$tower] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Tower','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Flat<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('flat',[''=>'',$flat] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Flat','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Resident Type<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('resident_type',[''=>'',$resident_type] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Resident Type','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-lg btn-outline-secondary">Reset</button>
                            @if($mode == 'CREATE')
                                <button type="submit" class="btn btn-lg btn-primary">Save</button>
                                @endif
                            @if($mode == 'EDIT')
                                <button type="submit" class="btn btn-lg btn-primary">Update</button>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-lg btn-danger">Delete</button>
                            @endif
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
