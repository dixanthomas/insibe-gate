@extends('layouts.dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->

        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($visitor, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}


                        <div class="form-group">
                            <label class="form-label" >Visitor Name<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('name',null, ['class' => 'form-control ', 'placeholder'=>'Visitor Name','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Arrival Date <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-left">
                                            <em class="icon ni ni-calendar"></em>
                                        </div>
                                        {!! Form::text('closing_date',null, ['class' => 'form-control date-picker ', 'placeholder'=>'Tender Closing Date','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Arrival Time <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-left">
                                            <em class="icon ni ni-clock"></em>
                                        </div>
                                        {!! Form::text('closing_time',null, ['class' => 'form-control  time-picker ', 'placeholder'=>'Tender Closing Time','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label class="form-label" >Visitor Mobile<span>*</span></label>
                            <div class="form-control-wrap">
                                <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                {!! Form::number('price',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Visitor Mobile Number','required' =>'required']) !!}
                            </div>
                        </div>
                                                
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >ID Proof <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select ID Proof','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Registration Number <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::number('signup_fee',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Registration Number','required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Resident Mobile<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Search Resident Mobile Number','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Resident Name<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Search Resident Name','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-lg btn-outline-secondary">Reset</button>
                            @if($mode == 'CREATE')
                                <button type="submit" class="btn btn-lg btn-primary">Save</button>
                                @endif
                            @if($mode == 'EDIT')
                                <button type="submit" class="btn btn-lg btn-primary">Update</button>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-lg btn-danger">Delete</button>
                            @endif
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
