@extends('layouts.guest-dashboard')

@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">

                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">
                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                {{-- <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Go Back</span></a> --}}
                                <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><span>Go Back</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->   
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block nk-block-lg">

        <div class="row g-gs">
            <div class="col-lg-8">
                <div class="card card-bordered h-100">
                    <div class="card-inner">

                        {!! Form::model($msg, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}


                        {{-- <div class="form-group">
                            <label class="form-label" >Security Name<span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::text('name',null, ['class' => 'form-control ', 'placeholder'=>'Security Guard Name','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Address<span>*</span></label>
                            <div class="form-control-wrap">
                                <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                {!! Form::text('name',null, ['class' => 'form-control ', 'placeholder'=>'Address','required' =>'required']) !!}
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >District <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select District','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Town <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Town','class' => 'form-control form-select', 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Mobile<span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::number('price',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Mobile Number','required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Mail Id <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::text('name',null, ['class' => 'form-control ', 'placeholder'=>'Mail Id','required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                       <div class="form-group">
                            <label class="form-label" >Message</label>
                            <div class="form-control-wrap">
                                {!! Form::textarea('message',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Package Description']) !!}
                            </div>
                        </div>

                        {{-- <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >ID Proof <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select ID Proof','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Registration Number <span>*</span></label>
                                    <div class="form-control-wrap">
                                        <!-- <div class="form-text-hint"><span class="overline-title">INR</span></div> -->
                                        {!! Form::number('signup_fee',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Registration Number','required' =>'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <!-- <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Invoice Period <span>*</span></label>
                                    <div class="form-control-wrap">
                                        {!! Form::number('invoice_period',null, ['class' => 'form-control','placeholder'=>'Invoice Period','required' =>'required']) !!}
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" >Invoice Interval <span>*</span></label>
                                    <div class="form-control-wrap">
                                        {!! Form::select('invoice_interval',[''=>'','hour'=>'Hour','day'=>'Day','week'=>'Week','month'=>'Month'] ,null, ['data-parsley-errors-container' => '#invoice-errors','data-placeholder' => 'Select  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                        <div id="invoice-errors"></div>
                                    </div>

                                </div>
                            </div>
                        </div> -->

                        <!-- <div class=" row gy-4">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" > Trial Period <span>*</span></label>
                                    <div class="form-control-wrap">
                                        {!! Form::number('trial_period',null, ['class' => 'form-control','placeholder'=>'Invoice Period','required' =>'required']) !!}

                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-label" > Trial Interval <span>*</span></label>
                                    <div class="form-control-wrap">
                                        {!! Form::select('trial_interval',[''=>'','hour'=>'Hour','day'=>'Day','week'=>'Week','month'=>'Month'] ,null, ['data-parsley-errors-container' => '#trial-errors','data-placeholder' => 'Select  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                        <div id="trial-errors"></div>
                                    </div>

                                </div>
                            </div>
                        </div> -->

                        <!-- <div class="form-group mt-4">
                            <label class="form-label" > Currency <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('currency',[''=>'','INR'=>'INR'] ,null, ['data-parsley-errors-container' => '#currency-errors','data-placeholder' => 'Select Currency','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="currency-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label" > Status <span>*</span></label>
                            <div class="form-control-wrap">
                                {!! Form::select('status',[''=>'','1'=>'Active','0'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select  Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                                <div id="status-errors"></div>
                            </div>
                        </div> -->

                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-lg btn-outline-secondary">Reset</button>
                            @if($mode == 'CREATE')
                                <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                @endif
                            @if($mode == 'EDIT')
                                <button type="submit" class="btn btn-lg btn-primary">Update</button>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-lg btn-danger">Delete</button>
                            @endif
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div><!-- .nk-block -->


@endsection
