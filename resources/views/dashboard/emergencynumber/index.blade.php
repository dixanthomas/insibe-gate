@extends('layouts.guest-dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    {{-- <p>You have total {{ $emergency_no->count() }} Numbers.</p> --}}
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">
                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                {{-- <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Go Back</span></a> --}}
                                <a href="{{ url('/') }}" class="btn btn-primary d-none d-md-inline-flex"><span>Go Back</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                {{-- <div class="nk-tb-col"><span class="sub-text">Resident ID</span></div> --}}
                <div class="nk-tb-col tb-col-md"><span class="sub-text"> Tower Name</span></div>
                <div class="nk-tb-col"><span class="sub-text">Title</span></div>
                <div class="nk-tb-col"><span class="sub-text">Name</span></div>
                <div class="nk-tb-col"><span class="sub-text">Number</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($emergency_no ) > 0)
                @foreach($emergency_no as $emergency)
                    <div class="nk-tb-item">
                        {{-- <div class="nk-tb-col"> --}}
                            {{-- <a href="{{ url('/dashboard/resident/'.$resident->id.'/edit') }}">
                                <span class="tb-lead">   {{ $resident->id }} </span>
                            </a> --}}
<!-- {{--                            @foreach($tender->categories as $category )--}}
{{--                                <a href="{{ url('dashboard/categories/'.$category->id.'/edit') }}">--}}
{{--                                    <span class="badge badge-secondary">{{ $category->title }}</span>--}}

{{--                                </a>--}}

{{--                            @endforeach--}} -->
                        {{-- </div> --}}

                        <div class="nk-tb-col tb-col-lg">
                            {{-- <span> {{ $emergency->tower_id }}</span> --}}
                            <span> {{ $emergency->tower_name }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $emergency->title }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $emergency->name }}</span>
                        </div>
                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $emergency->contact_no }}</span>
                        </div>
                    </div><!-- .nk-tb-item -->

                @endforeach
            @else
                    <h5>NO EMERGENCY NUMBER FOR YOUR TOWER</h5>
            @endif


        </div><!-- .nk-tb-list -->
        {{-- <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    <!-- {{ $residents->links() }} -->

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card --> --}}
    </div>

@endsection
