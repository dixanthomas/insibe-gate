@extends('layouts.guest-dashboard')

@section('content')

    <div class="nk-content-wrap">

        <div class="nk-block">
            <div class="card card-bordered">
                <div class="card-aside-wrap">
                    <div class="card-inner card-inner-lg">
                        <div class="nk-block-head nk-block-head-lg">
                            <div class="nk-block-between">
                                <div class="nk-block-head-content">
                                    <h4 class="nk-block-title">Personal Information</h4>
                                    <div class="nk-block-des">
                                        <p>Basic info, like your name and address, that you use on Nio Platform.</p>
                                    </div>
                                </div>
                                <div class="nk-block-head-content align-self-start d-lg-none">
                                    <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em
                                            class="icon ni ni-menu-alt-r"></em></a>
                                </div>
                            </div>
                        </div><!-- .nk-block-head -->
                        <div class="nk-block">
                            <div class="nk-data data-list">
                                <div class="data-head">
                                    <h6 class="overline-title">Basics</h6>
                                </div>
                                <div class="card h-100">
                                    <div class="modal-body">

                                        {!! Form::model(null, ['method' => '', 'data-parsley-validate', 'url' => '', 'class' => 'horizontal-form', 'files' => 'true', 'enctype' => 'multipart/form-data']) !!}

                                        <div class="form-group">
                                            <label class="form-label">Enter Old Password</label>
                                            <div class="form-control-wrap">
                                                {!! Form::text('message',  auth()->user()->name , ['class' => 'form-control', 'rows' => 1, 'placeholder' => 'Name']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Enter New Password</label>
                                            <div class="form-control-wrap">
                                                {!! Form::email('message',  auth()->user()->email , ['class' => 'form-control', 'rows' => 1, 'placeholder' => 'Message Description', 'disabled'=>'true']) !!}                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label class="form-label">Phone Number</label>
                                            <div class="form-control-wrap">
                                                {!! Form::text('message', auth()->user()->mobile , ['class' => 'form-control', 'rows' => 1, 'placeholder' => 'Phone Number']) !!}                                            </div>
                                        </div> --}}
                                        
                                        <div class="form-group mt-4">
                                            <button type="submit"
                                                class="btn btn-lg btn-outline-secondary">Reset</button>
                                            <button type="submit"
                                                class="btn btn-lg btn-primary">Save</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div><!-- data-list -->
                        </div><!-- .nk-block -->
                    </div>
                    <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg"
                        data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                        <div class="card-inner-group" data-simplebar>
                            <div class="card-inner">
                                <div class="user-card">
                                    <div class="user-avatar bg-primary">
                                        <span>AB</span>
                                    </div>
                                    <div class="user-info">
                                        <span class="lead-text">{{ ucfirst(Auth::user()->name) }}</span>
                                        <span class="sub-text">{{ Auth::user()->email }}</span>
                                    </div>
                                    <div class="user-action">
                                        <div class="dropdown">
                                            <a class="btn btn-icon btn-trigger mr-n2" data-toggle="dropdown" href="#"><em
                                                    class="icon ni ni-more-v"></em></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <ul class="link-list-opt no-bdr">
                                                    <li><a href="#"><em class="icon ni ni-camera-fill"></em><span>Change
                                                                Photo</span></a></li>
                                                    <li><a href="#"><em class="icon ni ni-edit-fill"></em><span>Update
                                                                Profile</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .user-card -->
                            </div><!-- .card-inner -->

                            <div class="card-inner p-0">
                                <ul class="link-list-menu">
                                    <li><a class="active" href="{{ url('/user/profile/basic-info') }}"><em
                                                class="icon ni ni-user-fill-c"></em><span>Personal Infomation</span></a>
                                    </li>
                                    <li><a href="{{ url('/user/profile/manage-address') }}"><em
                                                class="icon ni ni-bell-fill"></em><span>Manage Addresses</span></a></li>
                                    <li><a href="{{ url('/user/profile/gst-details') }}"><em
                                                class="icon ni ni-activity-round-fill"></em><span>GSTIN Details</span></a>
                                    </li>
                                    <li><a href="{{ url('/user/profile/change-password') }}"><em
                                                class="icon ni ni-lock-alt-fill"></em><span></span>Change Password</a></li>
                                    <li><a href="{{ url('/user/profile/manage-interests') }}"><em
                                                class="icon ni ni-lock-alt-fill"></em><span>Manage Interests</span></a>
                                    </li>

                                </ul>
                            </div><!-- .card-inner -->
                        </div><!-- .card-inner-group -->
                    </div><!-- card-aside -->
                </div><!-- .card-aside-wrap -->
            </div><!-- .card -->
        </div><!-- .nk-block -->

    </div>
@endsection
