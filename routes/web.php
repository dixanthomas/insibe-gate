<?php

use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Act;
use App\Models\Floor;
use App\Models\Resident;
use App\Models\Section;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use Rinvex\Subscriptions\Models\PlanFeature;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/packages', function() {
    $packages= \Rinvex\Subscriptions\Models\Plan::all();
    return view('packages',compact('packages'));
});


Route::group(['middleware' => 'auth'], function () {
    // Admin Dashboard
    Route::get('/dashboard', function () {
        return view('dashboard.index');
    });
    Route::get('/user/profile', function () {
        return view('user.profile');
    });
    Route::get('/user/profile/basic-info', function () {
        return view('user.basic-info');
    });
    // Route::get('/user/profile/change-password', function () {
    //     return view('/user.changepassword');
    // });
    Route::get('/profile', function () {
        $data=[
            'url'=>'/user-update',
            'method'=>'PUT'
        ];
        return view('user.basic-info',$data);
    });
    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/services', \App\Http\Controllers\TenderController::class);
    Route::resource('dashboard/packages', \App\Http\Controllers\PackageController::class);
    Route::resource('dashboard/society', \App\Http\Controllers\SocietyController::class);
    Route::resource('dashboard/security', \App\Http\Controllers\SecurityController::class);
    // Route::resource('dashboard/visitor', \App\Http\Controllers\VisitorController::class);
    Route::resource('dashboard/community', \App\Http\Controllers\CommunityController::class);


//    Route::resource('/residents', \App\Http\Controllers\ResidentController::class);
    Route::resource('/addflat', \App\Http\Controllers\ResidentController::class);
    Route::resource('/messagetosecurity', \App\Http\Controllers\MessageSecurityController::class);
//    Route::resource('/emergency-numbers', \App\Http\Controllers\SocietyController::class);
    // Route::resource('/localserviceproviders', \App\Http\Controllers\LocalServiceProviderController::class);
    // Route::resource('/notices', \App\Http\Controllers\NoticeController::class);
    Route::resource('/addvisitor', \App\Http\Controllers\VisitorController::class);
    Route::resource('/alert_security', \App\Http\Controllers\AlertSecurityController::class);
    // Route::resource('/userdetail', \App\Http\Controllers\VisitorController::class);




    //================================ :: User Routes :: ================================

    Route::get('/user-update', function () {
        return hey; 
    });

    Route::get('/', function() {
        $flat=App\Models\Resident::where('user_id',auth()->user()->id)->get();
        // return $flat->count();
//        $city = App\Models\City::pluck('name','id');
        $locations = App\Models\Locations::pluck('name','id');
        $society = App\Models\Society::pluck('name','id');
        $blocks = App\Models\Block::pluck('name','id');
        $flats = App\Models\Flat::pluck('name','id');
//        $resident_type = App\Models\ResidentType::pluck('role','id');
        $visitors=\App\Models\Resident::join('visitors','visitors.resident_id','=','residents.id')
                                       ->join('users','users.id','=','residents.user_id')
                                       ->select('visitors.name')
                                       ->where('residents.user_id',auth()->user()->id)
                                       ->distinct()->pluck('visitors.name','visitors.name');
       
            $flat_name=\App\Models\Flat::join('residents','residents.flat_id','=','flats.id')
                                        ->where('residents.user_id',auth()->user()->id)->pluck('flats.name','flats.id');
  
            return view('user.dashboard',compact('flats','flat','flat_name','locations','society','blocks','visitors'));
    });

    Route::get('/emergency-numbers', function () {
        // $flat=Resident::where('user_id',auth()->user()->id)->get('flat_id');
        // $floor=\App\Models\Flat::find($flat[0]->flat_id)->floor;
        // $block=\App\Models\Floor::find($floor->block_id)->block;
        // return $floor->block_id;
        // $society=\App\Models\Block::find($block->society_id)->societies;

        $emergency_no= \App\Models\Resident::join('flats','flats.id','=','residents.flat_id')
            ->join('floors','floors.id','=','flats.floor_id')
            ->join('blocks','blocks.id','=','floors.block_id')
            ->join('societies','societies.id','=','blocks.society_id')
            ->where('residents.user_id',auth()->user()->id)
            ->get(['societies.name','societies.phone01','societies.phone02','societies.email']);
        // return $emergency_no;
        $data = [
            'page_title' => 'Emergency Numbers'
        ];

        return view('user.emergency',compact('emergency_no'),$data );
    });
    Route::get('/all-residents', function () {

        $residents =  \App\Models\User::join('residents','residents.user_id','=','users.id')
            ->join('flats','flats.id','=','residents.flat_id')
            ->join('floors','floors.id','=','flats.floor_id')
            ->join('blocks','blocks.id','=','floors.block_id')
            ->whereIn('blocks.id',
                \App\Models\Resident::join('flats','flats.id','=','residents.flat_id')
                    ->join('floors','floors.id','=','flats.floor_id')
                    ->join('blocks','blocks.id','=','floors.block_id')
                    ->where('residents.user_id',auth()->user()->id)
                    ->pluck('blocks.id')
               )
            ->where('users.id','!=',auth()->user()->id)
            ->get(['users.name','users.mobile','flats.name as flat_name','users.email','blocks.name as block_name']);
        // return $residents;

        // return view('dashboard.temp',compact('t'));
        $data = [
            'page_title' => 'Your Tower Residents'
        ];

        return view('user.residents',compact('residents'),$data );
    });
    Route::get('/local-service-providers', function () {

        $localservices =  \App\Models\LocalServiceProvider::join('blocks','blocks.id','=','local_service_providers.block_id')
                ->whereIn('blocks.id',
                \App\Models\Resident::join('flats','flats.id','=','residents.flat_id')
                    ->join('floors','floors.id','=','flats.floor_id')
                    ->join('blocks','blocks.id','=','floors.block_id')
                    ->where('residents.user_id',auth()->user()->id)
                    ->pluck('blocks.id')
               )
            ->get(['local_service_providers.name','local_service_providers.contact','local_service_providers.description','local_service_providers.service','blocks.name as block_name']);
        // return $localservices;

        // return view('dashboard.temp',compact('t'));
        $data = [
            'page_title' => 'Your Tower Residents'
        ];

        return view('user.localservices',compact('localservices'),$data );
    });
    Route::get('/notices', function () {

        $notices =  \App\Models\Notice::join('societies','societies.id','=','notices.society_id')
                ->whereIn('society_id',
                \App\Models\Resident::join('flats','flats.id','=','residents.flat_id')
                    ->join('floors','floors.id','=','flats.floor_id')
                    ->join('blocks','blocks.id','=','floors.block_id')
                    ->join('societies','societies.id','=','blocks.society_id')
                    ->where('residents.user_id',auth()->user()->id)
                    ->pluck('societies.id')
               )
            ->orderBy('notices.created_at', 'desc')
            ->orderBy('notices.priority', 'desc')
            ->get(['notices.title','notices.notice','societies.name','notices.priority']);
        

        // return view('dashboard.temp',compact('t'));
        $data = [
            'page_title' => 'Your Tower Residents'
        ];

        return view('user.notices',compact('notices'),$data );
    });

    Route::get('/verification', function(){

        $data = [
            'page_title' => 'Complete your KYC process'
        ];

        return view('user.profile',$data );
    });

});

//SUPER ADMIN

Route::get('/users', function(){
    $users = \App\Models\User::all();
    return $users;
});

Route::get('/locations', function(){
    $locations = \App\Models\Locations::all();
    return $locations;
});

Route::get('/societies', function(){
    $socities = \App\Models\Society::all();
    return $socities;
});

Route::get('/blocks', function(){
    $blocks = \App\Models\Society::all();
    return $blocks;
});

Route::get('/GetSocieties', function(){

    $societies = \App\Models\Locations::find(3)->societies;
    return $societies;
});
Route::get('/GetFlats', function(){

    $societies = \App\Models\Flat::find(5)->floor;
    return $societies;
});

Route::get('/GetResidents', function(){

    $residents = \App\Models\Resident::all();

    return $residents;


});






//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/migrate', function(){
    Artisan::call('migrate');
    return '<h1>MIGRATE</h1>';
});
