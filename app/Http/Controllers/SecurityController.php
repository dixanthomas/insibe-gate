<?php

namespace App\Http\Controllers;

use App\Models\security;
use Illuminate\Http\Request;

class SecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $securities =  Security::paginate(15);

        $data = [
            'page_title' => 'Manage Security'
        ];

        return view('dashboard.security.index',compact('securities'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'security' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/security',
            'mode' => 'CREATE',
            'page_title' => 'Add a New Security'
        ];
        return view('dashboard.security.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\security  $security
     * @return \Illuminate\Http\Response
     */
    public function show(security $security)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\security  $security
     * @return \Illuminate\Http\Response
     */
    public function edit(security $security)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\security  $security
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, security $security)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\security  $security
     * @return \Illuminate\Http\Response
     */
    public function destroy(security $security)
    {
        //
    }
}
