<?php

namespace App\Http\Controllers;

use App\Models\AlertSecurity;
use Illuminate\Http\Request;

class AlertSecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $resident_id = \App\Models\Resident::select('id')->where('user_id', auth()->user()->id)
            ->where('flat_id', $request->get('flatnum'))
            ->get()->FirstorFail();
        if($request->get('alerttype')==1)
        $msg='Fire';
        elseif($request->get('alerttype')==2)            
        $msg='Stuck in lift';
        elseif($request->get('alerttype')==3)
        $msg='Animal threat';
        elseif($request->get('alerttype')==4)
        $msg='Visitor threat';
        else{
            $msg=$request->get('message');
        }
        // return $request->input();
        try {
            $alertmsg = new AlertSecurity();
            $alertmsg->resident_id=$resident_id->id;

            $alertmsg->alert_msg=$msg;
            // return $alertmsg;
            $alertmsg->save();

            return redirect('/')->with('success', 'Resident Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AlertSecurity  $alertSecurity
     * @return \Illuminate\Http\Response
     */
    public function show(AlertSecurity $alertSecurity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AlertSecurity  $alertSecurity
     * @return \Illuminate\Http\Response
     */
    public function edit(AlertSecurity $alertSecurity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AlertSecurity  $alertSecurity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AlertSecurity $alertSecurity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AlertSecurity  $alertSecurity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlertSecurity $alertSecurity)
    {
        //
    }
}
