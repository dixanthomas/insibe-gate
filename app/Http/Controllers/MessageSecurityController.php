<?php

namespace App\Http\Controllers;

use App\Models\MessageSecurity;
use Illuminate\Http\Request;

class MessageSecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $resdient_id = \App\Models\Resident::select('id')->where('user_id', auth()->user()->id)
            ->where('flat_id', $request->get('flatnum'))
            ->get();
        // return $resdient_id[0]->id;

            try {
            $msg = new MessageSecurity();
            $msg->message = $request->get('message');
            $msg->resident_id = $resdient_id[0]->id;

            $msg->save();

            return redirect('/')->with('success', 'Resident Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MessageSecurity  $messageSecurity
     * @return \Illuminate\Http\Response
     */
    public function show(MessageSecurity $messageSecurity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MessageSecurity  $messageSecurity
     * @return \Illuminate\Http\Response
     */
    public function edit(MessageSecurity $messageSecurity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MessageSecurity  $messageSecurity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MessageSecurity $messageSecurity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MessageSecurity  $messageSecurity
     * @return \Illuminate\Http\Response
     */
    public function destroy(MessageSecurity $messageSecurity)
    {
        //
    }
}
