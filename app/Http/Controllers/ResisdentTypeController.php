<?php

namespace App\Http\Controllers;

use App\Models\ResisdentType;
use Illuminate\Http\Request;

class ResisdentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResisdentType  $resisdentType
     * @return \Illuminate\Http\Response
     */
    public function show(ResisdentType $resisdentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ResisdentType  $resisdentType
     * @return \Illuminate\Http\Response
     */
    public function edit(ResisdentType $resisdentType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ResisdentType  $resisdentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResisdentType $resisdentType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResisdentType  $resisdentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResisdentType $resisdentType)
    {
        //
    }
}
