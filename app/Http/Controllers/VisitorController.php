<?php

namespace App\Http\Controllers;

use App\Models\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $resdient_id = \App\Models\Resident::select('id')
        ->where('user_id', auth()->user()->id)        
            ->where('flat_id', $request->get('flatnum'))->get();
        try {
            $visitor = new Visitor();
            // $visitor->title=$request->get('title');
            if ($request->get('addvisitorradiobtn') == 1)
                $name = $request->get('newname');
            else
                $name = $request->get('oldname');

            $visitor->name = $name;
            $visitor->phone = $request->get('phone');
            $visitor->resident_id = $resdient_id[0]->id; 
            $visitor->flat_id=$request->get('flatnum');
            if ($request->get('entrytype') == 'once') {
                $visitor->entrytype = 'once';
                $visitor->fromdate = $request->get('fromdate');
                $visitor->fromtime = date('Y-m-d H:i', strtotime($request->get('date') . $request->get('fromtime')));
                $visitor->totime = date('Y-m-d H:i', strtotime($request->get('date') . $request->get('fromtime')) + 3600 * $request->get('validefor'));
            }
            else {
                $visitor->entrytype = 'frequent';
                $visitor->fromdate = $request->get('fromdate');
                $visitor->todate = $request->get('todate');
            }
            // return $visitor;
            // return $request->input();
           

            $visitor->save();

            return redirect('/')->with('success', 'Resident Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Visitor $visitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitor $visitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitor $visitor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitor $visitor)
    {
        //
    }
}
