<?php

namespace App\Http\Controllers;

use App\Models\LocalServiceProvider;
use Illuminate\Http\Request;

class LocalServiceProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LocalServiceProvider  $localServiceProvider
     * @return \Illuminate\Http\Response
     */
    public function show(LocalServiceProvider $localServiceProvider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LocalServiceProvider  $localServiceProvider
     * @return \Illuminate\Http\Response
     */
    public function edit(LocalServiceProvider $localServiceProvider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LocalServiceProvider  $localServiceProvider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalServiceProvider $localServiceProvider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LocalServiceProvider  $localServiceProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocalServiceProvider $localServiceProvider)
    {
        //
    }
}
