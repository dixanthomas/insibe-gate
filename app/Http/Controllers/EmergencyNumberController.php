<?php

namespace App\Http\Controllers;

use App\Models\EmergencyNumber;
use Illuminate\Http\Request;

class EmergencyNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $flat_id = \App\Models\Resident::where('user_id',auth()->user()->id)->pluck('flat_id')->first();
        // $tower_id = \App\Models\TowerFlat::where('flat_id',$flat_id)->pluck('tower_id')->first();
        // $tower_name =\App\Models\Tower::where('id',$tower_id)->pluck('name')->first();
        // $emergency_no =  EmergencyNumber::where('tower_id',$tower_id)->pluck('contact_no')->first();

        $emergency_no= \App\Models\Resident::join('tower_flats','tower_flats.flat_id','=','residents.flat_id')
                                        ->join('emergency_numbers','emergency_numbers.tower_id','=','tower_flats.tower_id')
                                        ->join('towers','towers.id','=','tower_flats.tower_id')
                                        ->where('residents.user_id',auth()->user()->id)
                                        ->get(['emergency_numbers.contact_no','emergency_numbers.title','emergency_numbers.name','towers.name as tower_name']);
        // dd($emergency_no->count());
        // if($emergency_no->count()==0)
        //     return redirect()->back()->with('success', 'Resident Updated Successfully!');

        // ddd($emergency_no);
        // return view('dashboard.temp',compact('t'));
        // ddd($flat_id,$tower_id,$tower_name,$emergency_no);

        $data = [
            'page_title' => 'Emergency Numbers'
        ];

        return view('dashboard.emergencynumber.index',compact('emergency_no'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmergencyNumbers  $emergencyNumbers
     * @return \Illuminate\Http\Response
     */
    public function show(EmergencyNumbers $emergencyNumbers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmergencyNumbers  $emergencyNumbers
     * @return \Illuminate\Http\Response
     */
    public function edit(EmergencyNumbers $emergencyNumbers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmergencyNumbers  $emergencyNumbers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmergencyNumbers $emergencyNumbers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmergencyNumbers  $emergencyNumbers
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmergencyNumbers $emergencyNumbers)
    {
        //
    }
}
