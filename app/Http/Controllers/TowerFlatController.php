<?php

namespace App\Http\Controllers;

use App\Models\TowerFlat;
use Illuminate\Http\Request;

class TowerFlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TowerFlat  $towerFlat
     * @return \Illuminate\Http\Response
     */
    public function show(TowerFlat $towerFlat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TowerFlat  $towerFlat
     * @return \Illuminate\Http\Response
     */
    public function edit(TowerFlat $towerFlat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TowerFlat  $towerFlat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TowerFlat $towerFlat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TowerFlat  $towerFlat
     * @return \Illuminate\Http\Response
     */
    public function destroy(TowerFlat $towerFlat)
    {
        //
    }
}
