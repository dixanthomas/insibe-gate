<?php

namespace App\Http\Controllers;

use App\Models\city_society;
use Illuminate\Http\Request;

class CitySocietyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country_id=0)
    {
        $scocieties['data']=\App\Models\Society::Select('id','name')
                                        ->where('country_id',$country_id)
                                        ->get();
        return $scocieties;
        return response()->json($scocieties);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\city_society  $city_society
     * @return \Illuminate\Http\Response
     */
    public function show(city_society $city_society)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\city_society  $city_society
     * @return \Illuminate\Http\Response
     */
    public function edit(city_society $city_society)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\city_society  $city_society
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, city_society $city_society)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\city_society  $city_society
     * @return \Illuminate\Http\Response
     */
    public function destroy(city_society $city_society)
    {
        //
    }
}
