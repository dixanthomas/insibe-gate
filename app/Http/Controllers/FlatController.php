<?php

namespace App\Http\Controllers;

use App\Models\Flat;
use App\Models\City;
use App\Models\Society;
use App\Models\Tower;
use App\Models\User;
use App\Models\ResidentType;
use App\Models\Resident;
use Illuminate\Http\Request;

class FlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TO-DO how to disp the flats of a user**************************************************
        $flats =  Flat::paginate(15);

        $data = [
            'page_title' => 'Manage Flat'
        ];

        return view('dashboard.flat.index',compact('flats'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city = City::pluck('name','id');
        $society = Society::pluck('name','id');
        $tower = Tower::pluck('name','id');
        $flat = Flat::pluck('name','id');
        $resident_type = ResidentType::pluck('role','id');
        $data = [
            'flat' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/flat',
            'mode' => 'CREATE',
            'page_title' => 'Add a New Flat'
            
        ];
        return view('dashboard.flat.edit',compact('city','society','tower','flat','resident_type'),$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ddd(User::where("email",auth()->user()->email)->pluck('id')->first());
        try {
            $resident = new Resident();

            $resident->flat_id=$request->get('flat');
            $resident->user_id=User::where("email",auth()->user()->email)->pluck('id')->first();
            $resident->resident_type_id=$request->get('resident_type');
            
            $resident->save();

            return redirect('/')->with('success', 'Resident Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flat  $flat
     * @return \Illuminate\Http\Response
     */
    public function show(Flat $flat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flat  $flat
     * @return \Illuminate\Http\Response
     */
    public function edit(Flat $flat)
    {
        $resident = Resident::where('id', $id)->firstOrFail();
            
        $data = [
            'resident' => $resident,
            'formMethod' => 'PUT',
            'url' => 'dashboard/resident/'.$id,
            'mode' => 'EDIT',
            'page_title' => ' Edit '.$resident->name
           
        ];

        return view('dashboard.resident.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flat  $flat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flat $flat)
    {
        try {
            // $resident = Post::find($id);
            $resident = Resident::where('id', $id)->firstOrFail();
            $resident->name=$request->get('name');
            $resident->flat=$request->get('flat');
            $resident->mobile=$request->get('mobile');
            $resident->email=$request->get('email');
            
            $resident->update();

            return redirect()->back()->with('success', 'Resident Updated Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flat  $flat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flat $flat)
    {
        try {
            $resident = Resident::where('id', $id)->firstOrFail();
            $resident->delete();
            return redirect('dashboard/resident')->with('success', 'Resident Created Successfully!');
            // return redirect()->back()->with('success', 'Resident Deleted Successfully!');
            // return hello;
            } catch (\Exception $e) {
                return $e;
            }
    }
}
