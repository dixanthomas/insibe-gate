<?php

namespace App\Http\Controllers;

use App\Models\Resident;
use Illuminate\Http\Request;

class ResidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'resident' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/resident',
            'mode' => 'CREATE',
            'page_title' => 'Add a New Resident'
        ];
        return view('dashboard.resident.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->input();
        try {
            $resident = new Resident();
            $resident->flat_id=$request->get('flat');
            $resident->user_id=auth()->user()->id;
            $resident->resident_type=$request->get('resident_type');
            // $resident->email=$request->get('email');
            
            $resident->save();

            return redirect('/')->with('success', 'Resident Created Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function show(Resident $resident)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $resident = Resident::where('id', $id)->firstOrFail();
            
        $data = [
            'resident' => $resident,
            'formMethod' => 'PUT',
            'url' => 'dashboard/resident/'.$id,
            'mode' => 'EDIT',
            'page_title' => ' Edit '.$resident->name
        ];

        return view('dashboard.resident.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        try {
            // $resident = Post::find($id);
            $resident = Resident::where('id', $id)->firstOrFail();
            $resident->name=$request->get('name');
            $resident->flat=$request->get('flat');
            $resident->mobile=$request->get('mobile');
            $resident->email=$request->get('email');
            
            $resident->update();

            return redirect()->back()->with('success', 'Resident Updated Successfully!');
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resident  $resident
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        $resident = Resident::where('id', $id)->firstOrFail();
        $resident->delete();
        return redirect('dashboard/resident')->with('success', 'Resident Created Successfully!');
        // return redirect()->back()->with('success', 'Resident Deleted Successfully!');
        // return hello;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
