<?php

namespace App\Observers;

use App\Models\resident;

class ResidentObserver
{
    /**
     * Handle the resident "created" event.
     *
     * @param  \App\Models\resident  $resident
     * @return void
     */
    public function created(resident $resident)
    {

        // if (auth()->check()){
        //     $resident->created_by_user_id=auth()->id();
        //     $resident->save();
        // }
    }

    /**
     * Handle the resident "updated" event.
     *
     * @param  \App\Models\resident  $resident
     * @return void
     */
    public function updated(resident $resident)
    {
        //
    }

    /**
     * Handle the resident "deleted" event.
     *
     * @param  \App\Models\resident  $resident
     * @return void
     */
    public function deleted(resident $resident)
    {
        //
    }

    /**
     * Handle the resident "restored" event.
     *
     * @param  \App\Models\resident  $resident
     * @return void
     */
    public function restored(resident $resident)
    {
        //
    }

    /**
     * Handle the resident "force deleted" event.
     *
     * @param  \App\Models\resident  $resident
     * @return void
     */
    public function forceDeleted(resident $resident)
    {
        //
    }
}
