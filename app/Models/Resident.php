<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenantable;

class Resident extends Model
{
    use HasFactory, MultiTenantable;

    public function flats(){
        return $this->hasMany(Flat::class);
    }
}
 