<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Society extends Model
{
    use HasFactory;

    public function locations()
    {
        return $this->belongsTo(Locations::class);
    }
    public function blocks()
    {
        return $this->hasMany(Block::class,'society_id');
    }
}
