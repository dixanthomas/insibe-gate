<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{
    use HasFactory;

    // use \Znck\Eloquent\Traits\BelongsToThrough;

    public function societies()
    {
        // return $this->belongsToThrough(related:Society::class,through:Tower::classs);
    }

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

}
