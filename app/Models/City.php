<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    public function societies(){
        return $this->hasMany(related:Society::class);
    }
    public function towers(){
        return $this->hasManyThrough(related:Tower::class, through:Society::class);
    }
}
