<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;

    public function floors()
    {
        return $this->hasMany(Floor::class,'block_id');
    }
    public function societies()
    {
        return $this->belongsTo(Society::class);
    }
}
