<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Models\Resident;
use App\Observers\ResidentObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Schema::defaultStringLength(191);
        // Resident::observe(classes:ResidentObserver::class);
            // view->composer('layouts.guest-dashboard',function($view){
            //     $view->with('temp','testing ');
            // });

        // $flat=App\Models\Resident::where('user_id',auth()->user()->id)-get();
        // View::share('flat',$flat);
    }
}
