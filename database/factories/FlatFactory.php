<?php

namespace Database\Factories;

use App\Models\Floor;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'floor_id'=> Floor::all()->random()->id,
            'name'=> 'FLAT -'.$this->faker->unique()->numberBetween(1, 100)
        ];
    }
}
