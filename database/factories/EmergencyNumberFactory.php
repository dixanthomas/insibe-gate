<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmergencyNumberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title'=>$this->faker->title,
            'name'=>$this->faker->lastName,
            'contact_no'=>$this->faker->phoneNumber,        
            'tower_id'=>\App\Models\Tower::all()->random()->id
        ];
    }
}
