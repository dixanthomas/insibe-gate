<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\City;
use App\Models\Tower;
use App\Models\Flat;
use App\Models\Society;

class TowerFlatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tower_id'=>Tower::all()->random()->id,
            'flat_id'=>Flat::all()->random()->id
        ];
    }
}
