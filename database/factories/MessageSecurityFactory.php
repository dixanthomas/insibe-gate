<?php

namespace Database\Factories;

use App\Models\Resident;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Security;

class MessageSecurityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'guard_id'=>Security::factory(),
            'message'=>$this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'resident_id'=>Resident::all()->random()->id
        ];
    }
}
