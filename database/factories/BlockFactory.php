<?php

namespace Database\Factories;

use App\Models\Locations;
use App\Models\Society;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlockFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'society_id'=> Society::all()->random()->id,
            'name'=> $this->faker->buildingNumber,
        ];
    }
}
