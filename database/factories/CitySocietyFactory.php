<?php

namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;

class CitySocietyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'city_id' => \App\Models\City::all()->random()->id,
            'Society_id' => \App\Models\Society::all()->random()->id,
        ];
    }
}
