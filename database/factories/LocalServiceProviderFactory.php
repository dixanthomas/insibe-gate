<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LocalServiceProviderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName,
            'contact' => $this->faker->e164PhoneNumber,
            'description'=>$this->faker->text($maxNbChars = 150),
            'service' => 'cleaning',
            'block_id' => \App\Models\Block::all()->random()->id
        ];
    }
}
