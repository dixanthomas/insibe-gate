<?php

namespace Database\Factories;

use App\Models\Block;
use Illuminate\Database\Eloquent\Factories\Factory;

class FloorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'block_id'=> Block::all()->random()->id,
            'name'=> 'FLOOR -'.$this->faker->unique()->numberBetween(1, 100),
        ];
    }
}
