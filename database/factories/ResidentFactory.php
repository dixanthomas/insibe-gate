<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Flat;
use App\Models\ResidentType;

class ResidentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'user_id'=>User::factory(),
            'user_id' => \App\Models\User::all()->random()->id,
            'flat_id' => \App\Models\Flat::all()->random()->id,
            'resident_type' => 'Owner',
            'connect_with_residents' => true,

        ];
    }
}
