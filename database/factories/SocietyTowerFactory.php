<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\City;
use App\Models\Tower;
use App\Models\Flat;
use App\Models\Society;

class SocietyTowerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'society_id'=>Society::all()->random()->id,
            'tower_id'=>Tower::all()->random()->id
        ];
    }
}
