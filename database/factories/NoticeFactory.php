<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NoticeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'notice'=> $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'society_id'=>\App\Models\Society::all()->random()->id,
            'priority'=>'0'
        ];
    }
}
