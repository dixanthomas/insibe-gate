<?php

namespace Database\Factories;

use App\Models\Locations;
use App\Models\Tower;
use Illuminate\Database\Eloquent\Factories\Factory;

class SocietyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'location_id'=> Locations::all()->random()->id,
            'name'=> $this->faker->buildingNumber,
            'description'=> $this->faker->text(),
            'addressLine01'=> $this->faker->address,
            'addressLine02'=>$this->faker->address,
            'email'=> $this->faker->email,
            'phone01'=> $this->faker->phoneNumber,
            'phone02'=> $this->faker->phoneNumber,
            'city'=>$this->faker->city,
            'zip'=> $this->faker->postcode ,
            'status'=>'0',

        ];
    }
}
