<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTowerFlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tower_flats', function (Blueprint $table) {
            $table->id();
            $table->foreignId(column:'tower_id')->nullable()->constrained(table:'towers')->onDelete('cascade');
            $table->foreignId(column:'flat_id')->nullable()->constrained(table:'flats')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tower_flats');
    }
}
