<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocietytowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('society_towers', function (Blueprint $table) {
            $table->id();
            $table->foreignId(column:'society_id')->nullable()->constrained(table:'societies');
            $table->foreignId(column:'tower_id')->nullable()->constrained(table:'towers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('societytowers');
    }
}
