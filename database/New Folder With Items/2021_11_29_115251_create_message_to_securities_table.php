<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageToSecuritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_to_securities', function (Blueprint $table) {
            $table->id();            
            $table->foreignId(column:'guard_id')->nullable()->constrained(table:'securities');
            $table->string('message');
            $table->foreignId(column:'resident_id')->nullable()->constrained(table:'residents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_to_securities');
    }
}
