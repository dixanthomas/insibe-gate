<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdFlatIdToResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('residents', function (Blueprint $table) {
            $table->boolean('connect_with_residents')->default(TRUE);
            $table->string('notes')->nullable();
            $table->foreignId(column:'user_id')->nullable()->constrained(table:'users');
            $table->foreignId(column:'flat_id')->nullable()->constrained(table:'flats');
            $table->foreignId(column:'resident_type_id')->nullable()->constrained(table:'resident_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('residents', function (Blueprint $table) {
            //
        });
    }
}
