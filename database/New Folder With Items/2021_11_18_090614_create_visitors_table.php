<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->id();
            // $table->string('title');
            $table->string('name');
            $table->string('phone');
            $table->date('fromdate')->nullable();
            $table->date('todate')->nullable();
            $table->timestamp('fromtime')->nullable();
            $table->timestamp('totime')->nullable();
            $table->string('entrytype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
