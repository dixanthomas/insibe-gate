<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitySocietiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_societies', function (Blueprint $table) {
            $table->id();
            $table->foreignId(column:'society_id')->nullable()->constrained(table:'societies')->onDelete('cascade');
            $table->foreignId(column:'city_id')->nullable()->constrained(table:'cities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_societies');
    }
}
