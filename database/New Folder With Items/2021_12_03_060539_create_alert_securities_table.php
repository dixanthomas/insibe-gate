<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertSecuritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_securities', function (Blueprint $table) {
            $table->id();
            $table->foreignId(column:'resident_id')->nullable()->constrained(table:'residents');
            $table->foreignId(column:'security_id')->nullable()->constrained(table:'securities');
            $table->string('alert_msg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_securities');
    }
}
