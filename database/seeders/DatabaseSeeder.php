<?php

namespace Database\Seeders;

use App\Models\Locations;
use Database\Factories\CitySocietyFactory;

use Illuminate\Database\Seeder;

use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        \App\Models\User::factory(10)->create();
        \App\Models\Locations::factory(10)->create();
        \App\Models\Society::factory(30)->create();
        \App\Models\Block::factory(20)->create();
        \App\Models\Resident::factory(200)->create();

//        \App\Models\User::factory(10)->create();
//        \App\Models\Locations::factory(10)->create();
//        \App\Models\Society::factory(30)->create();
//          \App\Models\Block::factory(20)->create();
        //  \App\Models\Floor::factory(20)->create();
        //  \App\Models\Flat::factory(20)->create();
        // \App\Models\Resident::factory(20)->create();
        // \App\Models\LocalServiceProvider::factory(20)->create();
        // \App\Models\LocalServiceProvider::factory(20)->create();
        // \App\Models\MessageSecurity::factory(10)->create();

////
//        Location::create([
//             'name'=>'Kochi'
//         ]);
//        Location::create([
//             'name'=>'Bangalore'
//         ]);
//        Location::create([
//             'name'=>'Chennai'
//         ]);
//        Society::create([
//            'name'=>'Hoyasla Nestor'
//        ]);
//        Society::create([
//            'name'=>'Hoyasala Dezire'
//        ]);
//        Society::create([
//            'name'=>'Hoyasala EVM'
//        ]);

    //    \App\Models\Notice::factory(10)->create();
//
//
//
//


    }
}
